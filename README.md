#RSA_AES_Restful
restful API 是无的状态的，要保证安全就每次都需要发送验证身份，
所以用网页cookie+session的不行，要用app提交的明文username和请求的url+GUID ，
 url和guid要经过与服务器一样的对称加密算法用password加密；服务器接收到包后，
 根据username查询数据库里的密码，
用密码和对称的解密算法对（url+GUID）解密，
得到了GUID服务器在全局的集合里找是否存在GUID 如果存在，
则验证不通过，不存在则加入集合并通过验证。如果找不到用户，或者解密错误，都被认为是验证不通过
 
 
 生成公钥私钥对过程： 
            生成私钥：openssl genrsa -out rsa_private_key.pem 1024 
            根据私钥生成公钥： openssl rsa -in rsa_private_key.pem -out rsa_public_key.pem -pubout 
            这时候的私钥还不能直接被使用，需要进行PKCS#8编码： 
            openssl pkcs8 -topk8 -in rsa_private_key.pem -out pkcs8_rsa_private_key.pem -nocrypt 
            命令中指明了输入私钥文件为rsa_private_key.pem，输出私钥文件为pkcs8_rsa_private_key.pem，不采用任何二次加密（-nocrypt） 



加解密过程：
            1、将需要的参数mes取出key排序后取出value拼成字符串signdata 
            2、用signdata对商户私钥进行rsa签名，生成签名signn，并转base64格式 
            3、将签名signn插入到mes的最后生成新的data 
            4、用encryptkey16位常量对data进行AES加密后转BASE64,生成机密后的data 
            5、用对方公钥publickey对encryptkey16位常量进行RSA加密BASE64编码，生成加密后的encryptkey 
            6、将merchantaccount，第四部加密后的data，第五步加密后的encryptkey作为参数post请求给URL http://xxxx/xxx/api/xxx/xxx/xxx/xxx 
            7、返回的结果json传给data和encryptkey两部分，都为加密后的 
            8、用商户私钥对encryptkey进行RSA解密，生成解密后的encryptkey。参考方法：rsa_base64_decrypt 
            9、用解密后的encryptkey对data进行AES解密。参考方法：base64_aes_decrypt  
            
           def requestprocess(self,mesdata):
               values={}
               values['merchantaccount']=Gl.merchantaccount
               encryptkey = '1234567890123456'
               values['GUID']=random(8)
               data=self.aes_base64_encrypt(json.dumps(mesdata),encryptkey) 
               values['data']=data
               values['encryptkey']=self.rsa_base64_encrypt(encryptpassword,Gl.publickey) 
           GUIDS={}
           def result_decrypt(self,result):
               #从数据库中找到password = '1234567890123456'
               account = result['merchantaccount']
               with encryptpassword=db.find.where(username=account).password:
                     encryptkey = self.rsa_base64_encrypt(encryptpassword,Gl.publickey) 
                     if result['encryptkey']==encryptkey:
               		       kdata=result['data']
               		       rdata=self.base64_aes_decrypt(kdata,encryptpassword)
               		       print '解密后的data='+rdata
               		       session_key=result['GUID'] #避免重发攻击
               		       if GUIDS[account]！= session_key:
               		          GUIDS[account]=session_key
               	            return self.success(jsondata)
               	         return self.error()
               return self.error()
               		
               